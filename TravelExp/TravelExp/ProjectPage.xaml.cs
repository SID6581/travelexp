﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TravelExp
{
    public partial class ProjectPage : ContentPage
    {
        private ProjectDatabase _database;

        Project SelectedId;

        public ProjectPage(ProjectDatabase database)
        {
            InitializeComponent();
            
            _database = database;
            var _projectlist = _database.GetProjects();
            _projectList.ItemsSource = _projectlist;  
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            SelectedId = (Project)e.SelectedItem;
        }

        async void OnAdd(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ProjectEntryPage(this, _database));
            Refresh();
        }

        async void OnDelete(object sender, EventArgs e)
        {
            if (SelectedId == null)
            {
                await DisplayAlert("Hups!", "Tee valinta ensin", "OK");
                return;
            }
            var answer = await DisplayAlert("Kuluveloitus poistetaan pysyvästi.", "Oletko varma?", "Kyllä", "Ei");
            if (answer == true)
            {
                _database.DeleteProject(SelectedId);
                Refresh();
            }
        }

        async void OnEdit(object sender, EventArgs e)
        {
            if (SelectedId == null)
            {
                await DisplayAlert("Hups!", "Tee valinta ensin", "OK");
                return;
            }
            await Navigation.PushAsync(new ProjectEditPage(this, SelectedId, _database));
        }

        async void OnView(object sender, EventArgs e)
        {
            if (SelectedId == null)
            {
                await DisplayAlert("Hups!", "Tee valinta ensin", "OK");
                return;
            }
            await Navigation.PushAsync(new ProjectViewPage(this, SelectedId, _database));
        }
        public void Refresh()
        {
            _projectList.ItemsSource = _database.GetProjects();
        }

    }
}
