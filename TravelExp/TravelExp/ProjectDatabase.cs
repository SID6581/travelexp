﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace TravelExp
{
    public class ProjectDatabase

    {
        //private ProjectPage _parent;

        private SQLiteConnection _connection;

        //SQLite Project-tietokantaan yhdistäminen ja taulun luominen:

        public ProjectDatabase()
        {
            _connection = DependencyService.Get<ISQLite>().GetConnection();
            _connection.CreateTable<Project>();
        }

        //Koko taulun haku:

        public IEnumerable<Project> GetProjects()
        {
            return (from t in _connection.Table<Project>()
                    select t).ToList();
        }
        //Yhden tietueen haku:

        public Project GetProject(int id)
        {
            return _connection.Table<Project>().FirstOrDefault(t => t.ProjectID == id);
        }

        public int EditProject(Project aProject)
        {
            return _connection.Update(aProject);


        }
        //Tietueen poistometodi:

        public int DeleteProject(Project aProject)
        {
            return _connection.Delete(aProject);
            

        }
        // Tietueen lisäys-metodi:

        public void AddProject(string project, int km)
        {
            var newProject = new Project
            {
                ProjectName = project,
                Km = km, 
                CreatedOn = DateTime.Now
            };

            _connection.Insert(newProject);
        }
    }
}