﻿using System;
using SQLite.Net.Attributes;

namespace TravelExp
{
    [Table("Project")]
    public class Project
    {
        [PrimaryKey, AutoIncrement]
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string Sum { get; set; }
        public string ImagePath { get; set; }
        public int Km { get; set; }
        public DateTime CreatedOn { get; set; }

        public Project()
        {
        }
    }
}

