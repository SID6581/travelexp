﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TravelExp
{
    public partial class ProjectSelectorPage : ContentPage
    {
        private ProjectDatabase _database;
        Project SelectedId;
        private int km;

        public ProjectSelectorPage(ProjectDatabase database, double distance)
        {
            InitializeComponent();

            _database = database;
            var _projectlist = _database.GetProjects();
            _projectList.ItemsSource = _projectlist;
            //Lasketaan tietokannasta haettujen kulujen määrä
            var count = (_projectList.ItemsSource as List<Project>).Count;
            km = Convert.ToInt32(distance);
            //Jos kuluja ei ole - kutsutaan AddNew-metodia
            if (count < 1)
            {
                AddNew();
            }
        }

        private async void AddNew()
        {
            var answer = await DisplayAlert("Kuluveloituslista on tyhjä", "Tallennetaanko kilometrit uuteen kuluveloitukseen?", "Kyllä", "Ei");
            if (answer == true)
            {
                _database.AddProject("Uusi kuluveloitus", km);
                await Navigation.PopAsync();
            }
        }

        void OnSelection(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            SelectedId = (Project)e.SelectedItem;
        }

        async void OnSave(object sender, EventArgs e)
        {
            if (SelectedId == null)
            {
                await DisplayAlert("Hups!", "Tee valinta ensin", "OK");
                return;
            }
            var action = await DisplayActionSheet("Valitse miten haluat tallentaa kilometrit:", "Peruuta", null, "Lisätä edellisiin (" + SelectedId.Km + " km)", "Korvata edelliset");
            switch (action)
            {
                case "Peruuta":
                    break;
                case "Korvata edelliset":
                    SelectedId.Km = km;
                    break;
                default:
                    SelectedId.Km = SelectedId.Km + km;
                    break;
            }
            _database.EditProject(SelectedId);
            await Navigation.PopAsync();
        }

        public void Refresh()
        {
            _projectList.ItemsSource = _database.GetProjects();
            InitializeComponent();
        }

    }
}
