﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace TravelExp
{
    public class App : Application
 
    {
        

        public App()
        {
            

            MainPage = new NavigationPage(new TravelExp.MainPage())
            {
                // Määritetään väri palkkiin
                BarBackgroundColor = Color.FromHex("#6ddef2"), BarTextColor = Color.Black
            };

        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
