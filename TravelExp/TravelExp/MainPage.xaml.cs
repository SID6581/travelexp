﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TravelExp
{
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            InitializeComponent();
        }
        //Etusivun navigointi:

        async void LisaaKm(object sender, EventArgs e)
        {
            
            await Navigation.PushAsync(new DistancePage());
        }
        async void LisaaKulu(object sender, EventArgs e)
        {
            var database = new ProjectDatabase();
            await Navigation.PushAsync(new ProjectPage(database));
        }
        async void LisaaProjekti(object sender, EventArgs e)
        {
            await DisplayAlert("Auts!", "Valitettavasti toiminto ei sisälly alpha-versioon", "OK");
        }

    }
}
