﻿
using SQLite.Net;

//SQLite Interface (iOS)

namespace TravelExp
{
    public interface ISQLite
    {
        SQLiteConnection GetConnection();
    }
}
