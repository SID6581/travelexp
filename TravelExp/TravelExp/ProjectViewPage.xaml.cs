﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Diagnostics;

using Xamarin.Forms;

namespace TravelExp
{
    public partial class ProjectViewPage : ContentPage
    {
        private ProjectPage _parent;
        private ProjectDatabase _database;
        private Project SelectedId;

        public ProjectViewPage(ProjectPage parent, Project SelectedProject, ProjectDatabase database)
        {
            InitializeComponent();
            _parent = parent;
            _database = database;
            SelectedId = SelectedProject;
            BindingContext = SelectedProject;

            if (SelectedId.ImagePath != null)
            {
                Image2.Source = ImageSource.FromFile(SelectedId.ImagePath);
            }

        }
        async void OnEditClicked(object sender, EventArgs args)
        {

        }

        async void OnImageClicked(object sender, EventArgs args)
        {
        }



    }
}

