﻿
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;

using System.Diagnostics;

using Xamarin.Forms;

namespace TravelExp
{
    public partial class ProjectEditPage : ContentPage
    {
        private string path;
        private ProjectPage _parent;
        private ProjectDatabase _database;
        private Project SelectedId;
        
        public ProjectEditPage(ProjectPage parent, Project SelectedProject, ProjectDatabase database)
        {
            InitializeComponent();
            _parent = parent;
            _database = database;
            SelectedId = SelectedProject;
            BindingContext = SelectedProject;
            Debug.WriteLine("db:" +SelectedId.ImagePath);
            if (SelectedId.ImagePath != null) {
                Image1.Source = ImageSource.FromFile(SelectedId.ImagePath);
        }
            
        }
        async void OnSaveClicked(object sender, EventArgs args)
        {
            int km;
            if (Int32.TryParse(Km.Text, out km))
            {
                SelectedId.Description = Description.Text;
                SelectedId.ProjectName = ProjectName.Text;
                SelectedId.Sum = Sum.Text;
                SelectedId.Km = km;
                SelectedId.ImagePath = path;

                _database.EditProject(SelectedId);

                await Navigation.PopAsync();
                _parent.Refresh();
            }
            else
            {
                await DisplayAlert("Hups!", "Kilometreihin voi lisätä vain numerisia merkkejä", "OK");
            }
        }

        async void OnAddImageClicked(object sender, EventArgs args)
        {

            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("No Camera", ":( No camera available.", "OK");
                return;
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                SaveToAlbum = true,
                Name = "Kuitti" 
            });

            if (file == null)
                return;

            await DisplayAlert("File Location", file.Path, "OK");
            Image1.Source = ImageSource.FromStream(() => file.GetStream());
            path = file.Path;
            Debug.WriteLine("path:" + path);

        }

        void OnDeleteImageClicked(object sender, EventArgs args)
        {
            SelectedId.ImagePath = null;
            InitializeComponent();
            //File.Delete(path);
        }

    }
}