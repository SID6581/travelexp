﻿using System;
using Xamarin.Forms;

namespace TravelExp
{
    public class ProjectEntryPage : ContentPage
    {
        private ProjectPage _parent;
        private ProjectDatabase _database;

        public ProjectEntryPage(ProjectPage parent, ProjectDatabase database)
        {
            _parent = parent;
            _database = database;
            Title = "Syötä projektin nimi";

            var entry = new Entry();
            var button = new Button
            {
                Text = "Lisää"
            };


            button.Clicked += async (object sender, EventArgs e) => {
                var project = entry.Text;

                _database.AddProject(project, 0);

                await Navigation.PopAsync();


                _parent.Refresh();
            };

            Content = new StackLayout
            {
                Spacing = 20,
                Padding = new Thickness(20),
                Children = { entry, button },
            };
        }
    }
}