﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;

using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace TravelExp
{

    public partial class DistancePage : ContentPage
    {
        private double lat1, long1, lat2, long2, km, add;
        private DateTimeOffset time1;
        private DateTimeOffset time2;

        public DistancePage()
        {
            InitializeComponent();
        }

        private async void MeasureStartButton_OnClicked(object sender, EventArgs e)
        {
            await Begin();
        }

        private async void MeasureStopButton_OnClicked(object sender, EventArgs e)
        {
            await End();
        }
        private async Task End()
        {
            string finalkm = (km).ToString("0.#");
            StatusLabel.Text = "Mitattu matka: " + finalkm + " km";
            if (km > 0)
            {
                var answer = await DisplayAlert("Mittaustulos: " + finalkm, "Haluatko lopettaa mittauksen ja tallentaa?", "Kyllä", "Ei");
                if (answer == true)
                {
                   
                    var locator = CrossGeolocator.Current;

                    if (locator.IsListening)
                    {
                        await locator.StopListeningAsync();
                    }

                    var database = new ProjectDatabase();
                    await Navigation.PushAsync(new ProjectSelectorPage(database, km));
                }

            }
            else
            {
                await DisplayAlert("Mittaustulos on 0 km", "Ei tallennettavaa", "OK");
            }
        }
        private async Task Begin()
        {
            lat1 = 0;
            long1 = 0;
            lat2 = 0;
            long2 = 0;
            km = 0;
          
            try
            {
                var locator = CrossGeolocator.Current;

                if (locator.IsListening)
                {
                    await locator.StopListeningAsync();
                }
                StatusLabel.Text = "Haetaan GPS-signaalia...";
                
                await locator.StartListeningAsync(5000, 50, false);

                locator.AllowsBackgroundUpdates = true;
                
                locator.DesiredAccuracy = 50;
                locator.PositionError += (sender, args) =>
                {
                    if (args.Error == GeolocationError.Unauthorized)
                    {
                        StatusLabel.Text = "Ei yhteyttä: GPS-laitteen käyttölupa puuttuu";
                    }
                };

                var gpsPositionTask = locator.GetPositionAsync(5000);
                var gpsPosition = await gpsPositionTask;

                lat1 = gpsPosition.Latitude;
                long1 = gpsPosition.Longitude;
                time1 = gpsPosition.Timestamp;

                locator.PositionChanged += UpdateKm;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("exc: " + ex.Message);
            }
        }

        private void UpdateKm(object sender, PositionEventArgs e)

        {

            StatusLabel.Text = "Päivitetään sijaintia...";
            lat2 = e.Position.Latitude;
            long2 = e.Position.Longitude;
            time2 = e.Position.Timestamp;
            add = (CalculateKm2(time1, time2, e.Position.Speed));
            if (add > 0)
            {
                km = km+add;
            }
            KmLabel.Text = (km).ToString("0.#") + " km";
            SpeedLabel.Text = (e.Position.Speed * 3.6).ToString("N0")+" km/h";
            // Mittaussyklin uudelleenasetus:
            lat1 = lat2;
            long1 = long2;
            time1 = time2;
        }

        //Metodi1 joka laskee etäisyyden pituus- ja leveyskoordinaattien välillä ja palauttaa sen kilometreinä
        // --- toimii heikosti, koska ei siedä satunnaisia GPS-virhepiikkejä ---
        private double CalculateKm(double Lat1, double Long1, double Lat2, double Long2)
        {

            //Algoritmi v3:
            var baseRad = Math.PI * Lat1 / 180;
            var targetRad = Math.PI * Lat2 / 180;
            var theta = Long1 - Long2;
            var thetaRad = Math.PI * theta / 180;

            double dist =
                Math.Sin(baseRad) * Math.Sin(targetRad) + Math.Cos(baseRad) *
                Math.Cos(targetRad) * Math.Cos(thetaRad);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            return dist* dist * 1.609344;

        }
        //Metodi2 kertoo nopeuden kahden timestamp:in (GPS-päivitys) erotuksella ja palauttaa tuloksen kilometreinä:
        // --- toimii luotettavammin etäisyysmittauksessa kuin metodi1, heikkoutena nopeuden ajoittainen putoaminen nollaan jollain laitteilla ---
        private double CalculateKm2(DateTimeOffset Time1, DateTimeOffset Time2, double Speed)
        {
            return (Speed * (time2.Subtract(time1).TotalSeconds) / 1000);
        }


    }
}